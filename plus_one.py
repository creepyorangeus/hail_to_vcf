from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import warnings
import hail as hl
from datetime import datetime


warnings.simplefilter("ignore")


# Parse command line arguments
parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-im", "--input_mat", help="Folder with input matrix")
parser.add_argument("-i", "--input_vcf", help="Folder with VCF")
parser.add_argument("-o", "--output", default='output_plus_one', help="Path to output matrix")
parser.add_argument("-g", "--ref_gen", default='GRCh38', help="Reference genome: GRCh38 or GRCh37")
args = vars(parser.parse_args())

inmatrix_path = args["input_mat"]
in_vcf_path = args['input_vcf']
outdir = args["output"]
ref_gen = args["ref_gen"]

if __name__ == '__main__':
    start_time = datetime.now()
    hl.init()
    inmatrix = hl.read_matrix_table(inmatrix_path)
    in_vcf = hl.import_vcf(in_vcf_path, reference_genome=ref_gen)
    dataset_result = inmatrix.union_cols(in_vcf, row_join_type='inner')
    # Output the result
    dataset_result.write('{}.mt'.format(outdir), overwrite=True)
    print('Files have been merged into Hail matrix')
    hl.stop()
    print(datetime.now() - start_time)
