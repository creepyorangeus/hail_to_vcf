from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import warnings
import hail as hl
from datetime import datetime


warnings.simplefilter("ignore")


# Parse command line arguments
parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-i", "--infile", help="File with paths to vcf")
parser.add_argument("-o", "--outdir", default='output_merger', help="Hail matrix output folder")
parser.add_argument("-g", "--ref_gen", default='GRCh38', help="Reference genome: GRCh38 or GRCh37")
parser.add_argument("-j", "--join_type", default='inner', help="inner or outer join")
args = vars(parser.parse_args())

infile = args["infile"]
outdir = args["outdir"]
ref_gen = args["ref_gen"]
join_type = args["join_type"]


def read_vcf(x, gen=ref_gen):
    # read vcf
    hail_mx = hl.import_vcf(x, reference_genome=gen)
    return hail_mx

start_time = datetime.now()


if __name__ == '__main__':

    input_paths = []
    with open(infile, 'r') as f:
        for line in f:
            input_paths.append(line.strip())
    print('VCF_paths: ' + str(input_paths))

    hl.init()
    matrices = [read_vcf(x, gen=ref_gen) for x in input_paths]

    dataset_result = matrices[0]
    if len(input_paths) >= 2:
        for i in range(len(input_paths) - 1):
            dataset_result = dataset_result.union_cols(matrices[i+1], row_join_type=join_type)
    else:
        print('Only one VCF')

    # Output the result
    dataset_result.write('{}/merged_matrix.mt'.format(outdir), overwrite=True)
    print('Files have been merged into Hail matrix')
    hl.stop()
print(datetime.now() - start_time)
