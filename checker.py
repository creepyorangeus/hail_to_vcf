from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import warnings
import hail as hl
from datetime import datetime


warnings.simplefilter("ignore")


# Parse command line arguments
parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-im", "--input_mat", help="Folder with input matrix")
parser.add_argument("-q", "--query", help="locus query: example chr1:345678")
parser.add_argument("-o", "--output", default='output_checker', help="Path to output matrix")
parser.add_argument("-g", "--ref_gen", default='GRCh38', help="Reference genome: GRCh38 or GRCh37")
args = vars(parser.parse_args())

inmatrix_path = args["input_mat"]
query = args['query']
outdir = args["output"]
ref_gen = args["ref_gen"]

if __name__ == '__main__':
    start_time = datetime.now()
    hl.init()
    locus_query = hl.eval(hl.parse_locus(query, reference_genome=ref_gen))
    inmatrix = hl.read_matrix_table(inmatrix_path)
    agg_mat = (inmatrix.group_rows_by(inmatrix.locus, inmatrix.alleles).aggregate(
        n_non_ref=hl.agg.count_where(inmatrix.GT.is_non_ref())))
    agg_mat = agg_mat.filter_rows(agg_mat.locus == locus_query, keep=True)
    # Output the result
    agg_mat.write('{}.mt'.format(outdir), overwrite=True)
    hl.stop()
    print(datetime.now() - start_time)
